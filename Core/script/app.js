(function($) {

    // new WOW().init();
    //
    // $('body').scrollspy({
    //     target: '.sticky',
    //     offset: 100
    // });
    //
    // var $root = $('html, body');
    //
    // $('a[href^="#"]').click(function() {
    //     $root.animate({
    //         scrollTop: $($.attr(this, 'href')).offset().top
    //     }, 800);
    //
    //     return false;
    // });

    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky")
        } else {
            navbar.classList.remove("sticky");
        }
    }

// $(document).on('ready', function () {
    //     $("#slider").slick({
    //         dots: true,
    //         vertical: true,
    //         centerMode: true,
    //         slidesToShow: 4,
    //         slidesToScroll: 2
    //     });
    // })

    // $('#main').particleground({
    //     dotColor: '#333333',
    //     lineColor: '#333333',
    //     density: 7200,
    //     particleRadius: 4,
    //     maxSpeedX: 0.3,
    //     maxSpeedY: 0.3
    // });

    $('.main-text').shuffleLetters();

})(jQuery);